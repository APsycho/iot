package carner.josep.upf.iot.marc;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by jpepo7 on 23/5/16.
 */
public interface IGets {
    String getResult();

    int getResponse();

    void GetRestInventory(String devid) throws ParserConfigurationException, SAXException;
}
