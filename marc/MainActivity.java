package carner.josep.upf.iot.marc;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.io.StringWriter;
import java.io.PrintWriter;


public class MainActivity extends ActionBarActivity {

    Button c;
    Button b;
    EditText t;
    EditText t2;
    String s;
    String result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initialing DB
        db mydb = new DBHelper(this);

        Button c = (Button) findViewById(R.id.button);
        t= (EditText) findViewById(R.id.editText);
        //t2 = (EditText) findViewById(R.id.editText2);

        c.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Gets a = new Gets(MainActivity.this,"Start");
                try{
                    a.execute();
                }catch(Exception e){
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    e.printStackTrace();
                    //sw.toString(); // stack trace as a string
                    //t.setText(a.getResponse()+sw.toString()+a.getConcatat());
                }
                //s=a.getResult();
                //System.out.println("main: "+s+a.getResult()+a.result);
            }
        });

        Button b = (Button) findViewById(R.id.button2);

        b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Gets a = new Gets(MainActivity.this,"Stop");
                try{
                    a.execute();
                }catch(Exception e){
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    e.printStackTrace();
                    //sw.toString(); // stack trace as a string
                    //t.setText(a.getResponse()+sw.toString()+a.getConcatat());
                }
                //s=a.getResult();
                //System.out.println("main: "+s+a.getResult()+a.result);

            }
        });
        Button a1 = (Button) findViewById(R.id.button3);

        a1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Gets a = new Gets(MainActivity.this,"Inventory");
                try{
                    a.execute();
                }catch(Exception e){
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    e.printStackTrace();
                    //sw.toString(); // stack trace as a string
                    //t.setText(a.getResponse()+sw.toString()+a.getConcatat());
                }
                //s=a.getResult();
                //System.out.println("main: "+s+a.getResult()+a.result);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
