package carner.josep.upf.iot.marc;

	import android.app.Activity;
    import android.os.AsyncTask;
	import android.util.Log;
    import android.view.View;
    import android.widget.EditText;

	import java.io.BufferedInputStream;
	import java.io.BufferedReader;
    import java.io.IOException;
    import java.io.InputStream;
    import java.io.InputStreamReader;
    import java.io.PrintWriter;
    import java.io.StringReader;
    import java.net.HttpURLConnection;
    import java.net.MalformedURLException;
    import java.net.URL;

    import javax.xml.parsers.DocumentBuilder;
    import javax.xml.parsers.DocumentBuilderFactory;
    import javax.xml.parsers.ParserConfigurationException;

    import org.apache.http.HttpEntity;
    import org.apache.http.HttpResponse;
    import org.apache.http.client.ClientProtocolException;
    import org.apache.http.client.HttpClient;
    import org.apache.http.client.methods.HttpGet;
    import org.apache.http.impl.client.DefaultHttpClient;
    import org.json.JSONException;
    import org.json.JSONObject;

    import org.w3c.dom.Document;
    import org.w3c.dom.Element;
    import org.w3c.dom.Node;
    import org.w3c.dom.NodeList;
    import org.xml.sax.InputSource;
    import org.xml.sax.SAXException;
	
public class Gets extends AsyncTask<Void , Void, Void> implements IGets {

	String method;
    String result;
	String TAG="tags";
	int response;
    String url="http://192.168.43.144:3161/";

    MainActivity myAc;
		public Gets(MainActivity ac, String met){
            myAc=ac;
			method=met;

		}

    @Override
    protected Void doInBackground(Void...params) {

        JSONObject json = null;
        HttpClient httpclient = new DefaultHttpClient();
        if(method.equals("Start")){
            // Prepare a request object
            HttpGet httpget = new HttpGet(url+"devices/simulator/start");
            // Accept JSON
            httpget.addHeader("accept", "application/json");
            // Execute the request
            HttpResponse response;
            try {
                response = httpclient.execute(httpget);
                // Get the response entity
                HttpEntity entity = response.getEntity();
                // If response entity is not null
                if (entity != null) {
                    // get entity contents and convert it to string
                    InputStream instream = entity.getContent();
                    result= convertStreamToString(instream);
                    System.out.println(result);
                    // construct a JSON object with result
                    //json=new JSONObject("{"+result+"}");
                    // Closing the input stream will trigger connection release
                    instream.close();
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }/* catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
            // Return the json
        }else if(method.equals("Inventory")){
            // Prepare a request object
            HttpGet httpget = new HttpGet(url+"devices/simulator/inventory");
            // Accept JSON
            httpget.addHeader("accept", "application/json");
            // Execute the request
            HttpResponse response;
            try {
                response = httpclient.execute(httpget);
                // Get the response entity
                HttpEntity entity = response.getEntity();
                // If response entity is not null
                if (entity != null) {
                    // get entity contents and convert it to string
                    InputStream instream = entity.getContent();
                    result= convertStreamToString(instream);
                    System.out.println(result);
                    // Closing the input stream will trigger connection release
                    instream.close();
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }else if(method.equals("Stop")){
            // Prepare a request object
            HttpGet httpget = new HttpGet(url+"devices/simulator/stop");
            // Accept JSON
            httpget.addHeader("accept", "application/json");
            // Execute the request
            HttpResponse response;
            try {
                response = httpclient.execute(httpget);
                // Get the response entity
                HttpEntity entity = response.getEntity();
                // If response entity is not null
                if (entity != null) {
                    // get entity contents and convert it to string
                    InputStream instream = entity.getContent();
                    result= convertStreamToString(instream);
                    System.out.println(result);
                    // Closing the input stream will trigger connection release
                    instream.close();
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
    }

    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
    @Override
	protected void onPreExecute() {
		Log.i(TAG, "onPreExecute");
	}
	@Override
	 protected void onPostExecute(Void re) {
		Log.i(TAG, "onPostExecute");


        DocumentBuilder db = null;
        try {
            db = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(result));
            Document doc = db.parse(is);
            if(method.equals("Start")) {
                NodeList nList = doc.getElementsByTagName("response");

                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);
                    System.out.println("\nCurrent Element :" + nNode.getNodeName());
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;

                        System.out.println("OPERATION: " + eElement.getElementsByTagName("op").item(0).getTextContent());
                        System.out.println("STATUS: " + eElement.getElementsByTagName("status").item(0).getTextContent());

                        myAc.t.setText(eElement.getElementsByTagName("op").item(0).getTextContent() + "\n" + eElement.getElementsByTagName("status").item(0).getTextContent());
                    }
                }
            }else if(method.equals("Stop")){

                NodeList nList = doc.getElementsByTagName("response");

                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);
                    System.out.println("\nCurrent Element :" + nNode.getNodeName());
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;

                        System.out.println("OPERATION: " + eElement.getElementsByTagName("op").item(0).getTextContent());
                        System.out.println("STATUS: " + eElement.getElementsByTagName("status").item(0).getTextContent());

                        myAc.t.setText(eElement.getElementsByTagName("op").item(0).getTextContent() + "\n" + eElement.getElementsByTagName("status").item(0).getTextContent());
                    }
                }
            }else if(method.equals("Inventory")){
                int size=0;
                NodeList nList = doc.getElementsByTagName("inventory");
                //NodeList nList = doc.getElementsByTagName("item");
                String report="";
                for (int temp = 0; temp < nList.getLength(); temp++) {
                    //System.out.println("YOOOOOOOOOOOOOLO: "+nList.getLength());
                    Node nNode = nList.item(temp);
                    System.out.println("\nCurrent Element :" + nNode.getNodeName());
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        if (eElement.getElementsByTagName("size").item(0).getTextContent() != null) {
                            size = Integer.parseInt(eElement.getElementsByTagName("size").item(0).getTextContent());
                            System.out.println(eElement.getElementsByTagName("size").item(0).getTextContent());
                            report = "Number Of Tags: " + eElement.getElementsByTagName("size").item(0).getTextContent() + "\n";
                        }

                        /*for(int i=0;i<size;i++) {
                        report+=("Event : TAG"+"\n"+
                                "EPC : " + eElement.getElementsByTagName("epc").item(i).getTextContent()+"\n"+
                                "TimeStamp : " + eElement.getElementsByTagName("ts").item(i).getTextContent()+"\n"+
                                "DeviceId : " + eElement.getElementsByTagName("deviceId").item(i).getTextContent()+"\n"+
                                "Tid : " + eElement.getElementsByTagName("tid").item(i).getTextContent()+"\n\n" );


                        myAc.t.setText(report);
                        //}*/

                    }
                }
                    NodeList nList2 = doc.getElementsByTagName("item");
                    //NodeList nList = doc.getElementsByTagName("item");
                    //String report="";
                for (int temp2 = 0; temp2 < nList2.getLength(); temp2++) {
                        Node nNode2 = nList2.item(temp2);
                        System.out.println("\nCurrent Element :" + nNode2.getNodeName());
                        if (nNode2.getNodeType() == Node.ELEMENT_NODE) {
                            Element eElement = (Element) nNode2;
                        //for(int i=0;i<size;i++) {
                            report+=("Event : TAG"+"\n"+
                                    "EPC : " + eElement.getElementsByTagName("epc").item(0).getTextContent()+"\n"+
                                    "TimeStamp : " + eElement.getElementsByTagName("ts").item(0).getTextContent()+"\n"+
                                    "DeviceId : " + eElement.getElementsByTagName("deviceId").item(0).getTextContent()+"\n"+
                                    "Tid : " + eElement.getElementsByTagName("tid").item(0).getTextContent()+"\n\n" );


                            myAc.t.setText(report);
                        //}
                    }
                }
            }
        }catch (ParserConfigurationException e) {
             e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }


	}





@Override
public String getResult(){
    return result;
}
@Override
public int getResponse(){
	return response;
}


			
			
		


		@Override
        public void GetRestInventory(String devid) throws ParserConfigurationException, SAXException {

			  try {

				URL url = new URL("http://localhost:3161/devices/"+devid+"/inventory");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");

				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
							+ conn.getResponseCode());
				}

				BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

				String output;
				String concatat="";
				System.out.println("Output from Server .... \n");
				
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				InputSource is = new InputSource();
				
				//PrintWriter writer = new PrintWriter("/output.xml");
				
				while ((output = br.readLine()) != null) {
					//System.out.println(output);
					/* No vol tirar 
				    is.setCharacterStream(new StringReader(output));
				    Document doc = db.parse(is);
				    NodeList nodes = doc.getElementsByTagName("CATALOG");
		            System.out.println(nodes.item(0).getTextContent());	            
		            */
					
					//writer.write(output);
					concatat+=output;
					
				}				
				//writer.close();
				System.out.println(concatat);
				is.setCharacterStream(new StringReader(concatat));
			    Document doc = db.parse(is);
			    
				NodeList nList = doc.getElementsByTagName("item");
		         
		         for (int temp = 0; temp < nList.getLength(); temp++) {
		            Node nNode = nList.item(temp);
		            System.out.println("\nCurrent Element :" + nNode.getNodeName());
		            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		               Element eElement = (Element) nNode;
		               //System.out.println("Item : " + eElement.getAttribute("TITLE"));
		               System.out.println("Event : " + eElement.getElementsByTagName("class").item(0).getTextContent());
		               System.out.println("EPC : " + eElement.getElementsByTagName("epc").item(0).getTextContent());
		               System.out.println("TimeStamp : " + eElement.getElementsByTagName("ts").item(0).getTextContent());
		               System.out.println("DeviceId : " + eElement.getElementsByTagName("deviceId").item(0).getTextContent());
		               System.out.println("Hexepc : " + eElement.getElementsByTagName("hexepc").item(0).getTextContent());
		               System.out.println("Tid : " + eElement.getElementsByTagName("tid").item(0).getTextContent());
		              
		  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(0).getTextContent());
		  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(1).getTextContent());
		  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(2).getTextContent());
		  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(3).getTextContent());
		  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(4).getTextContent());
		  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(5).getTextContent());
		  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(6).getTextContent());
		  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(7).getTextContent());	
		              
		            }
		         }
				conn.disconnect();

			  } catch (MalformedURLException e) {

				e.printStackTrace();

			  } catch (IOException e) {

				e.printStackTrace();

			  }

			}

		
		
		
		
	}


