package carner.josep.upf.iot.rf;
	
	import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class FileParser implements IFileParser {

	public FileParser(){
		
	}
	@Override
	public void ParseDocInventory(){
	      try {	
	         File inputFile = new File("/...xml");
	         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         Document doc = dBuilder.parse(inputFile);
	         doc.getDocumentElement().normalize();
	         System.out.println("Root element :" 
	            + doc.getDocumentElement().getNodeName());
	         NodeList nList = doc.getElementsByTagName("item");
	         System.out.println("----------------------------");
	         for (int temp = 0; temp < nList.getLength(); temp++) {
	            Node nNode = nList.item(temp);
	            System.out.println("\nCurrent Element :" + nNode.getNodeName()+" "+temp);
	            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	               Element eElement = (Element) nNode;
	               System.out.println("Event : " + eElement.getElementsByTagName("class").item(0).getTextContent());
	               System.out.println("EPC : " + eElement.getElementsByTagName("epc").item(0).getTextContent());
	               System.out.println("TimeStamp : " + eElement.getElementsByTagName("ts").item(0).getTextContent());
	               System.out.println("DeviceId : " + eElement.getElementsByTagName("deviceId").item(0).getTextContent());
	               System.out.println("Hexepc : " + eElement.getElementsByTagName("hexepc").item(0).getTextContent());
	               System.out.println("Tid : " + eElement.getElementsByTagName("tid").item(0).getTextContent());

	              
	  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(0).getTextContent());
	  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(1).getTextContent());
	  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(2).getTextContent());
	  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(3).getTextContent());
	  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(4).getTextContent());
	  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(5).getTextContent());
	  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(6).getTextContent());
	  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(7).getTextContent());		            
	               
	               
	            }
	         }
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	   }
	
	
	@Override
	public void ParseDocDevices(){
	      try {	
	         File inputFile = new File("/../.xml");
	         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         Document doc = dBuilder.parse(inputFile);
	         doc.getDocumentElement().normalize();
	         System.out.println("Root element :" 
	            + doc.getDocumentElement().getNodeName());
	         NodeList nList = doc.getElementsByTagName("device");
	         System.out.println("----------------------------");
	         for (int temp = 0; temp < nList.getLength(); temp++) {
	            Node nNode = nList.item(temp);
	            System.out.println("\nCurrent Element :" + nNode.getNodeName()+" "+temp);
	            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	               Element eElement = (Element) nNode;
	               System.out.println("ID : " + eElement.getElementsByTagName("id").item(0).getTextContent());
	               System.out.println("IP : " + eElement.getElementsByTagName("ip").item(0).getTextContent());
	               System.out.println("MAC : " + eElement.getElementsByTagName("mac").item(0).getTextContent());
	               System.out.println("SERIAL : " + eElement.getElementsByTagName("serial").item(0).getTextContent());
	               System.out.println("FAMILY : " + eElement.getElementsByTagName("family").item(0).getTextContent());
	               System.out.println("CODE : " + eElement.getElementsByTagName("code").item(0).getTextContent());
	               System.out.println("IS ALIVE: " + eElement.getElementsByTagName("isAlive").item(0).getTextContent());
	               System.out.println("STATUS: " + eElement.getElementsByTagName("status").item(0).getTextContent());

	  		    
	               
	            }
	         }
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	   }
	}

