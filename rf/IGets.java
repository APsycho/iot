package carner.josep.upf.iot.rf;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by jpepo7 on 22/5/16.
 */
public interface IGets {
    void GetDevices() throws ParserConfigurationException, SAXException;

    void GetRestConnect(String devid, String method) throws ParserConfigurationException, SAXException;

    void GetRestInventory(String devid) throws ParserConfigurationException, SAXException;

    void GetRestLED(String devid, int led) throws ParserConfigurationException, SAXException;
}
