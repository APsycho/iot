package carner.josep.upf.iot.rf;

/**
 * Created by jpepo7 on 22/5/16.
 */
public interface IFileParser {
    void ParseDocInventory();

    void ParseDocDevices();
}
