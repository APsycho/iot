package carner.josep.upf.iot.rf;

	import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
	
public class Gets implements IGets {
	
		public Gets(){
			
		}		
		
		@Override
		public void GetDevices() throws ParserConfigurationException, SAXException{
			
			try {

				URL url = new URL("http://localhost:3161/devices");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");

				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
							+ conn.getResponseCode());
				}

				BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

				String output;
				String concatat="";
				System.out.println("Output from Server .... \n");
				
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				InputSource is = new InputSource();
				
				//PrintWriter writer = new PrintWriter("/output.xml");
				
				while ((output = br.readLine()) != null) {
					//System.out.println(output);
					
					//writer.write(output);
					concatat+=output;
					
				}				
				//writer.close();
				System.out.println(concatat);
				is.setCharacterStream(new StringReader(concatat));
			    Document doc = db.parse(is);
			    
				NodeList nList = doc.getElementsByTagName("devices");
		         
		         for (int temp = 0; temp < nList.getLength(); temp++) {
		            Node nNode = nList.item(temp);
		            System.out.println("\nCurrent Element :" + nNode.getNodeName());
		            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		               Element eElement = (Element) nNode;
		               //System.out.println("Item : " + eElement.getAttribute("TITLE"));
		               System.out.println("ID : " + eElement.getElementsByTagName("id").item(0).getTextContent());
		               System.out.println("IP : " + eElement.getElementsByTagName("ip").item(0).getTextContent());
		               System.out.println("MAC : " + eElement.getElementsByTagName("mac").item(0).getTextContent());
		               System.out.println("SERIAL : " + eElement.getElementsByTagName("serial").item(0).getTextContent());
		               System.out.println("FAMILY : " + eElement.getElementsByTagName("family").item(0).getTextContent());
		               System.out.println("CODE : " + eElement.getElementsByTagName("code").item(0).getTextContent());
		               System.out.println("IS ALIVE: " + eElement.getElementsByTagName("isAlive").item(0).getTextContent());
		               System.out.println("STATUS: " + eElement.getElementsByTagName("status").item(0).getTextContent());

		  		    
		            }
		         }
				conn.disconnect();

			  } catch (MalformedURLException e) {

				e.printStackTrace();

			  } catch (IOException e) {

				e.printStackTrace();

			  }

			}

		
		
		
			
			
			
		
		@Override
		public void GetRestConnect(String devid, String method) throws ParserConfigurationException, SAXException {
			URL url;
		  try {
			  if(method=="connect"){
				  	url = new URL("http://localhost:3161/devices/"+devid+"/connect");
			  }else{
					url = new URL("http://localhost:3161/devices/"+devid+"/start");
			  }
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}else{
				int responseCode = conn.getResponseCode();
				System.out.println("Response Code : " + responseCode);
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

			String output;
			String concatat="";
			System.out.println("Output from Server .... \n");
			
			
			
			while ((output = br.readLine()) != null) {
				System.out.println(output);			
				
				//concatat+=output+"\n";
				
			}				
			
			//System.out.println(concatat);
			
	         
			conn.disconnect();

		  } catch (MalformedURLException e) {

			e.printStackTrace();

		  } catch (IOException e) {

			e.printStackTrace();

		  }

		}

		@Override
		public void GetRestInventory(String devid) throws ParserConfigurationException, SAXException {

			  try {

				URL url = new URL("http://localhost:3161/devices/"+devid+"/inventory");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");

				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
							+ conn.getResponseCode());
				}

				BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

				String output;
				String concatat="";
				System.out.println("Output from Server .... \n");
				
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				InputSource is = new InputSource();
				
				//PrintWriter writer = new PrintWriter("/output.xml");
				
				while ((output = br.readLine()) != null) {
					//System.out.println(output);
					/* No vol tirar 
				    is.setCharacterStream(new StringReader(output));
				    Document doc = db.parse(is);
				    NodeList nodes = doc.getElementsByTagName("CATALOG");
		            System.out.println(nodes.item(0).getTextContent());	            
		            */
					
					//writer.write(output);
					concatat+=output;
					
				}				
				//writer.close();
				System.out.println(concatat);
				is.setCharacterStream(new StringReader(concatat));
			    Document doc = db.parse(is);
			    
				NodeList nList = doc.getElementsByTagName("item");
		         
		         for (int temp = 0; temp < nList.getLength(); temp++) {
		            Node nNode = nList.item(temp);
		            System.out.println("\nCurrent Element :" + nNode.getNodeName());
		            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		               Element eElement = (Element) nNode;
		               //System.out.println("Item : " + eElement.getAttribute("TITLE"));
		               System.out.println("Event : " + eElement.getElementsByTagName("class").item(0).getTextContent());
		               System.out.println("EPC : " + eElement.getElementsByTagName("epc").item(0).getTextContent());
		               System.out.println("TimeStamp : " + eElement.getElementsByTagName("ts").item(0).getTextContent());
		               System.out.println("DeviceId : " + eElement.getElementsByTagName("deviceId").item(0).getTextContent());
		               System.out.println("Hexepc : " + eElement.getElementsByTagName("hexepc").item(0).getTextContent());
		               //System.out.println("Tid : " + eElement.getElementsByTagName("tid").item(0).getTextContent());
		              
		  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(0).getTextContent());
		  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(1).getTextContent());
		  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(2).getTextContent());
		  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(3).getTextContent());
		  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(4).getTextContent());
		  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(5).getTextContent());
		  		       System.out.println("prop : " + eElement.getElementsByTagName("prop").item(6).getTextContent());
		  		       //System.out.println("prop : " + eElement.getElementsByTagName("prop").item(7).getTextContent());	
		              
		  		       if(eElement.getElementsByTagName("epc").item(0).getTextContent().equals("300833b2ddd901400000f231")){
		  		    	   GetRestLED("adrd-m4-100@192.168.2.151@00:80:a3:90:70:a2",1);
		  		       }else{
		  		    	   GetRestLED("adrd-m4-100@192.168.2.151@00:80:a3:90:70:a2",2);
		  		       }
		            }
		         }
				conn.disconnect();

			  } catch (MalformedURLException e) {

				e.printStackTrace();

			  } catch (IOException e) {

				e.printStackTrace();

			  }

			}

		
		

		@Override
		public void GetRestLED(String devid, int led) throws ParserConfigurationException, SAXException {

			  try {

				URL url = new URL("http://localhost:3161/devices/"+devid+"/setGPO/"+led+"/false");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");

				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
							+ conn.getResponseCode());
				}

				BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

				String output;
				String concatat="";
				System.out.println("Output from Server .... \n");
				
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				InputSource is = new InputSource();
				
				//PrintWriter writer = new PrintWriter("/output.xml");
				
				while ((output = br.readLine()) != null) {
					//System.out.println(output);
					/* No vol tirar 
				    is.setCharacterStream(new StringReader(output));
				    Document doc = db.parse(is);
				    NodeList nodes = doc.getElementsByTagName("CATALOG");
		            System.out.println(nodes.item(0).getTextContent());	            
		            */
					
					//writer.write(output);
					concatat+=output;
					
				}				
				//writer.close();
				System.out.println(concatat);
				
		         
				conn.disconnect();

			  } catch (MalformedURLException e) {

				e.printStackTrace();

			  } catch (IOException e) {

				e.printStackTrace();

			  }

			}

		
		
		
	}


